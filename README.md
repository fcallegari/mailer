# Decrizione

Webservice scritto in php utilizzato come webhook da MailDash per l'inoltro di email a database FileMaker tramite REST API.

### Come funziona

Lo script da chiamare è <code>[root]/ricevi.php</code> che quindi deve essere esposto.

MailDash viene impostato per utilizzare questo endpoint come webhook, a cui accodare il payload json che rappresenta il codice sorgente di un messaggio email.

Lo script acquisisce lo stream, lo decodifica, localizza il formato dela data all'interno dell'elemento headers e passa il risultato (ricodificato in json) a FileMaker.

### Come vengono passati i dati a FileMaker

Il passaggio dei dati a Filemaker avviene in questo modo:

- viene impostato un campo globale lato FM;
- viene eseguito uno script lato FM che elabora il dato nel campo globale ed esegue le azioni necessaria sul db FileMaker.

Le due azioni di cui sopra sono eseguite da altrettante funzioni php che utilizzano il protocollo DataAPI per colloquiare con FM.

Le funzioni sono depositate nel file di libreria <code>lib.php</code>.

### Configurazioni

Il file <code>config.php</code> deve essere completato con i dati relativi al db destinazione di FM, in particolare:

- nome del db
- autenticazione
- nome del formato FM che riceve la chiamata
- nome completamente qualificato del campo globale da impostare su FM
