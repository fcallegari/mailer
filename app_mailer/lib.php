<?php
require('config.php');

function login()
{
  $endpoint = HOST . '/fmi/data/v2/databases/' . DATABASE . '/sessions';
  $options = CURL_OPT;
  $options[CURLOPT_URL] = $endpoint;
  $options[CURLOPT_CUSTOMREQUEST] = 'POST';
  $options[CURLOPT_POSTFIELDS] = '{}';
  $options[CURLOPT_HTTPHEADER] = array(
    'Authorization: Basic ' . AUTH,
    'Content-Type: application/json'
  );
  $curl = curl_init();
  curl_setopt_array($curl, $options);
  $response = json_decode(curl_exec($curl));
  curl_close($curl);
  if ($response->messages[0]->code != "0") {
    throw new Exception("Errore connessione database", 501);
  }
  $_SESSION["fmtoken"] = $response->response->token;
  return $response;
}

function logout($token = NULL)
{
  // è possibile chiamarla con un parametro esplicito, per chiudere manualmente le sessioni in fase di test
  $token = $token ?? $_SESSION['fmtoken'];
  $endpoint = HOST . '/fmi/data/v2/databases/' . DATABASE . '/sessions/' . $token;
  $options = CURL_OPT;
  $options[CURLOPT_URL] = $endpoint;
  $options[CURLOPT_CUSTOMREQUEST] = 'DELETE';
  $options[CURLOPT_POSTFIELDS] = '{}';
  $options[CURLOPT_HTTPHEADER] = array(
    'Authorization: Basic ' . AUTH,
    'Content-Type: application/json'
  );

  $curl = curl_init();
  curl_setopt_array($curl, $options);

  $response = json_decode(curl_exec($curl));

  curl_close($curl);
  $_SESSION["fmtoken"] = "";
  return $response;
}

function impostaCampoGlobale($fieldName, $value)
{
  $endpoint = HOST . '/fmi/data/v2/databases/' . DATABASE . '/globals';
  $payload = (object) array(
    "globalFields" => (object) array(
      $fieldName => $value
    )
  );
  $options = CURL_OPT;
  $options[CURLOPT_URL] = $endpoint;
  $options[CURLOPT_CUSTOMREQUEST] = 'PATCH';
  $options[CURLOPT_POSTFIELDS] = json_encode($payload);
  $options[CURLOPT_HTTPHEADER] = array(
    'Authorization: Bearer ' . $_SESSION["fmtoken"],
    'Content-Type: application/json'
  );
  $curl = curl_init();
  curl_setopt_array($curl, $options);
  $response = json_decode(curl_exec($curl));
  curl_close($curl);
  return $response;
}

function eseguiFMScript($scriptName)
{
  $endpoint = HOST . '/fmi/data/v2/databases/' . DATABASE . '/layouts/' . LAYOUT . '/script/' . $scriptName;
  $options = CURL_OPT;
  $options[CURLOPT_URL] = $endpoint;
  $options[CURLOPT_CUSTOMREQUEST] = 'GET';
  $options[CURLOPT_POSTFIELDS] = '';
  $options[CURLOPT_HTTPHEADER] = array(
    'Authorization: Bearer ' . $_SESSION["fmtoken"]
  );
  $curl = curl_init();
  curl_setopt_array($curl, $options);
  $response = json_decode(curl_exec($curl));
  curl_close($curl);
  return $response;
}

function checkAuth()
{
  if (!isset($_SERVER["HTTP_AUTHORIZATION"])) {
  }
  $auth = base64_decode(explode(" ",  $_SERVER["HTTP_AUTHORIZATION"])[1]);
  [$user, $pass] = explode(":", $auth);
  if ($user !== CM_USER || $pass !== CM_PASS) {
    throw new Exception("Credenziali non corrette");
  }
}
