<?php
//http_response_code(503); //service unavailable -> blocco manuale per manutenzione
require('../../app_mailer/lib.php');

try {
    checkAuth();
} catch (Exception $e) {
    http_response_code(401);
    die($e->getMessage());
}

$postData = file_get_contents("php://input");
$postObj = json_decode($postData);
if (!isset($postObj)) {
    http_response_code(406); // not acceptable
    die("Manca body della richiesta");
}

// trasformazione data in formato FM dd/mm/yyyy hh:mm:ss
$date = date_create(
    $postObj->headers->date
);

$postObj->headers->date = date_format($date, "d/m/Y H:i:s");
try {
    login();
    impostaCampoGlobale(GLOBALE_RICEZIONE, json_encode($postObj));
    eseguiFMScript(SCRIPT_CREAZIONE);
    logout();
} catch (Exception $e) {
    echo ($e->getMessage());
    http_response_code($e->getCode());
}
